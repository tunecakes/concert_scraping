var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var datejs = require('datejs');

var fecha = new Date("2018-06-24");

for (var i =0; i < 180; i++) {
  fecha.addDays(-7);
  let urldate = fecha.getFullYear() + "-" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "-" + ("0" + fecha.getDate()).slice(-2);
  var url = 'https://www.billboard.com/charts/artist-100/' + urldate;

  console.log(fecha.getFullYear() + "-" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "-" + ("0" + fecha.getDate()).slice(-2));

  request(url, function(err, response, html){
    if(err){
      console.log(err)
      console.log(urldate);
      return
    }

      var $ = cheerio.load(html);
      var allItems = $("div.container").children();
      var items = ['NEWWEEK'];

      allItems.each(function(index){
        var result = allItems.eq(index).find("a.chart-row__artist").text().trim();
        if (result != '') {
          items.push(result)
        }
      })

      fs.appendFile("artists.txt", items, function(err){
        if(err){
          console.log(err)
          console.log(urldate)
          return
        } else {
          console.log("another one")
          console.log(urldate)
        }
      })
  })
}
