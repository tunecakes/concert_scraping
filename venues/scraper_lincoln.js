const moment = require('moment-timezone')
const scraper = require('../lib/scraper');

var url = 'http://lincolntheatre.com/events/';
var itemMapper = function(ele){
   let event = {
      location: "Lincoln Theater",
      date: ele.find("div.rhino-event-datebox-month").children().eq(0).text() + ", " +
      ele.find("div.rhino-event-datebox-date").children().eq(0).text(),
      title: ele.find("h2").children().eq(0).attr("title"),
      supportingActs: ele.find("h3.rhino-event-subheader").text(),
      showLink: ele.find("h2").children().eq(0).attr("href"),
      ticketLink: ele.find("[title='Buy Tickets']").attr("href"),
      time: ele.find("p.rhino-event-time").text()
   }

   // This removes the name of the day (i.e. Monday) from the beginning of the date string,
   // it removes "Doors: " from the beginning of the time string, and it remove the
   // trailing " /" from the end of the time string
   let dateString = event.date + " " + event.time 

   // Let moment parse this timezone-based date time
   let computedDate = moment.tz(dateString, "MMM, D h:mm a", "America/New_York")

   // If the current month is greater than the month of the computed date,
   // that means this event is set for the next calendar year. This means that
   // if this script is run in November 2018 and an event is in January 2019, moment will set the
   // event to be in January 2018, therefore we need to add one year.
   if (moment().month() > computedDate.month()) {
      computedDate.year(computedDate.year() + 1)
   }

   event.computedDate = computedDate.toISOString()
   return event
};

var findDomEle = function($){
  return $("div.vcalendar").children();
};

var lincolnScraper = function(callback){
  scraper(url, findDomEle, itemMapper, callback);
};

module.exports = lincolnScraper
