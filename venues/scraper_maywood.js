const moment = require('moment-timezone')
const scraper = require('../lib/scraper');

var url = 'http://www.themaywoodraleigh.com/';
var itemMapper = function(ele){
   let event = {
      location: "The Maywood",
      date: ele.find("div.showdate").text(),
      title:ele.find("div.showband1").text(),
      supportingActs: ele.find("div.showband2").text(),
      showLink: ele.find("p.detailslink").children().eq(0).attr("href"),
      ticketLink: ele.find("p.showgenre").find("a").last().attr("href"),
      time: ele.find("p.showtime").contents().eq(0).text()
   }

   // Get rid of the day name in the date and only grab the time from the start of the time string
   let dateString = event.date.substring(event.date.indexOf(" ") + 1, event.date.length) +
                     " " + event.time.substring(0, event.time.indexOf(" ")) 

   console.log(dateString)
   // Let moment parse this timezone-based date time
   let computedDate = moment(dateString, ["MMMM D, YYYY h:mma", "MMMM D, YYYY ha"]).tz("America/New_York")

   // If the current month is greater than the month of the computed date,
   // that means this event is set for the next calendar year. This means that
   // if this script is run in November 2018 and an event is in January 2019, moment will set the
   // event to be in January 2018, therefore we need to add one year.
   if (moment().month() > computedDate.month()) {
      computedDate.year(computedDate.year() + 1)
   }

   event.computedDate = computedDate
   return event
};

var findDomEle = function($){
   return $("table#showbox").children().children();
};

var maywoodscraper = function(callback){
   scraper(url, findDomEle, itemMapper, callback);
};

module.exports =  maywoodscraper
