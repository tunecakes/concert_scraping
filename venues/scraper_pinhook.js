const moment = require('moment-timezone')
const scraper = require('../lib/scraper');

var url = 'https://www.thepinhook.com/calendar/';
var itemMapper = function(ele){
   let event = {
      location: ele.find("h2.venue").text(),
      date: ele.find("h5.date").text(),
      title: ele.find("h1.headliners").text(),
      supportingActs: ele.find("h2.supports").eq(0).text() + ", "
                    + ele.find("h2.supports").eq(1).text() + ", "
                    + ele.find("h2.supports").eq(2).text() + ", ",
      showLink: "https://www.thepinhook.com" + ele.find("a").attr("href"),
      ticketLink: ele.find("h3.ticket-link").children().attr("href"),
      time: "N/A",
   }

   return event
};

var findDomEle = function($){
  return $("div.one-event");
};

var pinhookscraper = function(callback){
  scraper(url, findDomEle, itemMapper, callback);
};

module.exports =  pinhookscraper;
