var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');

var url = 'http://www.slimsraleigh.com';

request(url, function(err, response, html){
  if(!err){
    var $ = cheerio.load(html);
    var allItems =  $("div.wallpage").children();
    var showsArray = [];

    allItems.each(function(index){
      var show = {
      location: "Slims",
      date: allItems.eq(index).find("div.newsitemtitle").children().eq(0).text(),
      title: allItems.eq(index).find("h3.newsitemtitlelg").text(),
      // FIXME: supporting acts are squished together and not separated by commas
      supportingActs: allItems.eq(index).find("div.newsitemtitle").find("a").text(),
      showLink: "http://www.slimsraleigh.com",
      ticketLink: "http://www.slimsraleigh.com",
      // FIXME: This pulls the time and the cost. Need to find a way to pull just the time
      time: allItems.eq(index).find("div.newsitemtitle").children().eq(1).text()
      }
      showsArray.push(show)
    })

    fs.writeFile("output.txt", JSON.stringify(showsArray, null, 4), function(err){
      if(err){
        console.log(err)
      } else {
        console.log("Shows added to file")
      }
    })
  }
})
