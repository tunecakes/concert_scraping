const moment = require('moment-timezone')
const scraper = require('../lib/scraper')

var url = 'http://catscradle.com/venue/cats-cradle-back-room/'
var itemMapper = function(ele){
   let event = {
      location: ele.find("p.rhino-event-venue").children().eq(0).text(),
      date: ele.find("p.rhino-event-date").text().trim(),
      title: ele.find("h2.rhino-event-header").children().attr("title"),
      supportingActs: ele.find("h3").text(),
      showLink: ele.find("h2.rhino-event-header").children().attr("href"),
      ticketLink: ele.find("a.button").attr("href"),
      time: ele.find("p.rhino-event-time").eq(0).text().trim(),
   }

   // This removes the name of the day (i.e. Monday) from the beginning of the date string,
   // it removes "Doors: " from the beginning of the time string, and it remove the
   // trailing " /" from the end of the time string
   let dateString = event.date.substring(event.date.indexOf(", "), event.date.length) +
                     " " + event.time.substring(7).slice(0, -2) 

   // Let moment parse this timezone-based date time
   let computedDate = moment.tz(dateString, "MMMM D h:mm a", "America/New_York")

   // If the current month is greater than the month of the computed date,
   // that means this event is set for the next calendar year. This means that
   // if this script is run in November 2018 and an event is in January 2019, moment will set the
   // event to be in January 2018, therefore we need to add one year.
   if (moment().month() > computedDate.month()) {
      computedDate.year(computedDate.year() + 1)
   }

   event.computedDate = computedDate.toISOString()
   return event
}

var findDomEle = function($){
  return $("div.vcalendar").children()
}

var backcatscradle = function(callback){
  scraper(url, findDomEle, itemMapper, callback)
}

module.exports = backcatscradle
