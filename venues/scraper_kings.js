const moment = require('moment-timezone')
const scraper = require('../lib/scraper');

var url = 'http://www.kingsraleigh.com/';
var itemMapper = function(ele){
   let event = {
      location: "Kings",
      date: ele.children().eq(1).find("p.date").text(),
      title: ele.children().eq(1).children().eq(1).find("a").text(),
      supportingActs: ele.children().eq(1).find("h4").text(),
      showLink: ele.children().eq(1).children().eq(1).find("a").attr("href"),
      ticketLink: ele.children().eq(2).find("a.tickets").attr("href"),
      time: ele.children().eq(1).find("p:last-child").text(),
   }

   // Remove the day name for the date and remove the string "Doors: " from the front of time
   let dateString = event.date.substring(event.date.indexOf(", "), event.date.length) +
                    " " + event.time.substring(7, event.time.length) 

   // Let moment parse this timezone-based date time
   let computedDate = moment.tz(dateString, "MMMM D, YYYY h:mmA", "America/New_York")

   event.computedDate = computedDate.toISOString()
   return event
};

var findDomEle = function($){
  return  $("#Shows").children().children();
};

var kingsscraper = function(callback){
  scraper(url, findDomEle, itemMapper, callback);
};

module.exports = kingsscraper
