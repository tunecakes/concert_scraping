const moment = require('moment-timezone')
const scraper = require('../lib/scraper');

var url = 'https://www.deepsouththebar.com/calendar/';
var itemMapper = function(ele){
   let event = {
     location: ele.find("h2.venue").text(),
     date: ele.parent().find("h5.date").text(),
     title: ele.find("h1.headliners").eq(0).text(),
     supportingActs:   ele.find("h1.headliners").eq(1).text() + ", "
                     + ele.find("h1.headliners").eq(2).text() + ", "
                     + ele.find("h1.headliners").eq(3).text(),
     showLink: "https://www.deepsouththebar.com" + ele.find("a.url").attr("href"),
     ticketLink: ele.find("h3.ticket-link").children().attr("href"),
     time: ele.find("h3.start-time").text(),
   }

   // Grab just the numerical date and time
   let dateString = event.date.substring(event.date.indexOf(" "), event.date.length) +
                    " " + event.time 

   // Let moment parse this timezone-based date time
   let computedDate = moment.tz(dateString, "M/D h:mm a", "America/New_York")

   // If the current month is greater than the month of the computed date,
   // that means this event is set for the next calendar year. This means that
   // if this script is run in November 2018 and an event is in January 2019, moment will set the
   // event to be in January 2018, therefore we need to add one year.
   if (moment().month() > computedDate.month()) {
      computedDate.year(computedDate.year() + 1)
   }

   event.computedDate = computedDate.toISOString()
   return event
};

var findDomEle = function($){
  return $("div.one-event");
};

var deepsouthscraper = function(callback){
  scraper(url, findDomEle, itemMapper, callback);
};

module.exports =  deepsouthscraper;
