var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');

var url = 'http://wickedwitchraleigh.com/events/';

request(url, function(err, response, html){
  if(!err){
    var $ = cheerio.load(html);
    var allItems = $("div.tribe-events-loop").children();
    var showsArray = [];

    allItems.each(function(index){
      var show = {
      location: "The Wicked Witch",
      //FIXME: date and time come through
      date: allItems.eq(index).find("span.tribe-event-date-start").text(),
      title: allItems.eq(index).find("h4.entry-title").children().attr("title"),
      supportingActs: "N/A",
      showLink: allItems.eq(index).find("h4.entry-title").children().attr("href"),
      ticketLink: "N/A",
      //FIXME: date and time come trough
      time: allItems.eq(index).find("span.tribe-event-time").text()
      }
      showsArray.push(show)
    })

    fs.writeFile("output.txt", JSON.stringify(showsArray, null, 4), function(err){
      if(err){
        console.log(err)
      } else {
        console.log("Shows added to file")
      }
    })
  }
})
