module.exports = {
   catscradleBack: require('./scraper_catscradle_back'),
   catscradleMain: require('./scraper_catscradle_main'),
   deepsouth: require('./scraper_deepsouth'),
   kings: require('./scraper_kings'),
   lincoln: require('./scraper_lincoln'),
   maywood: require('./scraper_maywood'),
   // TODO: fix these
   //pinhook: require('./scraper_pinhook'),
   //pourhouse: require('./scraper_pourhouse'),
   //slims: require('./scraper_slims'),
   //wickedwitch: require('./scraper_wickedwitch'),
}

