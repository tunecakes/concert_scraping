const moment = require('moment-timezone')
const scraper = require('../lib/scraper');

var url = 'http://thepourhousemusichall.com/events/';
var itemMapper = function(ele){
  return {
    location: "Pour House Music Hall",
    date: ele.find("div.rhino-event-datebox-month").children().eq(0).text() + ", " + ele.find("div.rhino-event-datebox-date").children().eq(0).text(),
    title: ele.find("h2").children().eq(0).attr("title"),
    supportingActs: ele.find("h3.rhino-event-subheader").text(),
    showLink: ele.find("h2").children().eq(0).attr("href"),
    ticketLink: ele.find("[title='Buy Tickets']").attr("href"),
    time: ele.find("p.rhino-event-time").last().text().trim()
  }
};

var findDomEle = function($){
  return $("div.vcalendar").find("div.tribe-events-venue-1777");
};

var pourhousescraper = function(callback){
  scraper(url, findDomEle, itemMapper, callback);
};

module.exports =  pourhousescraper;
