const fs = require('fs')

const mkdirp = require('mkdirp')

const venues = require('./venues')

let outputDirectoryExists = false
mkdirp('./output/', (err) => {
   if (err) process.exit(1)
   outputDirectoryExists = true
})

Object.keys(venues).forEach(venue => {
   const venueScraper = venues[venue]

   venueScraper((err, events) => {
      if (err) {
         console.log("Failed to scrape events for " + venue, err)
         return
      }

      while(!outputDirectoryExists);

      fs.writeFile("./output/" + venue + "-events.json", JSON.stringify(events, null, 2), function(err){
         if(err){
            console.log("Failed to save events for " + venue, err)
            return
         }
         console.log("Successfully scraped " + venue)
      })
   })
})

