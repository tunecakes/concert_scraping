var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');

var url = 'https://ncartmuseum.org/calendar/type/performances_concerts';

request(url, function(err, response, html){
  if(!err){
    var $ = cheerio.load(html);
    var allItems = $("div.tribe-events-loop").children();
    var showsArray = [];

    allItems.each(function(index){
      var show = {
      location:
      date:
      title: allItems.eq(index).find("h2.media-heading").children().text(),
      supportingActs:
      showLink:
      ticketLink:
      time:
      }
      showsArray.push(show)
    })

    fs.writeFile("output.txt", JSON.stringify(showsArray, null, 4), function(err){
      if(err){
        console.log(err)
      } else {
        console.log("Shows added to file")
      }
    })
  }
})
