var request = require('request');
var cheerio = require('cheerio');

var scrape = function(url, findDomEle, itemMapper, callback){
   request(url, function(err, response, html){
      if (err) {
         return callback(err);
      }

      var $ = cheerio.load(html);
      var allItems = findDomEle($);
      var showsArray = []

      allItems.each(function functionName() {
         var ele = $(this)
         showsArray.push(itemMapper(ele))
      });
      callback(null, showsArray)
  })
}

module.exports = scrape;
